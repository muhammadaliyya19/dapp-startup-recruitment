// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.4.17;
// pragma experimental ABIEncoderv2;

contract Rekrutmen {
    address public admin;
    uint sum_rekrutmen;
    uint total_pelamar_all;
    uint jumlah_pelamar;

    function Rekrutmen() public {
        admin = msg.sender;
        sum_rekrutmen = 0;
        total_pelamar_all = 0;
        jumlah_pelamar = 0;
    }

    // Fungsi restrict / batas agar hanya bisa diakses oleh admin
    function Admin() public view returns(address) {
        return admin;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin);
        _;
    }

    // Set up struktur data Beasiswa
    struct Data_Rekrutmen{
        uint id;
        string judul;
        string deskripsi;
        string main_link;
        bool is_active;
        uint kuota;
        uint jumlah_pendaftar;
    }

    // Pemetaan atau memasukkan data tunggal beasiswa agar menjadi suatu kumpulan data beasiswa
    mapping(uint => Data_Rekrutmen) public List_Rekrutmen;

    function penambahanRekrutmen(string _judul, string _deskripsi, string _main_link, uint _kuota) public onlyAdmin() {
        Data_Rekrutmen memory newRekrutmen = Data_Rekrutmen({
            id : sum_rekrutmen,
            judul : _judul,
            deskripsi : _deskripsi,
            main_link : _main_link,
            is_active : true,
            kuota : _kuota,
            jumlah_pendaftar : 0
        });
        List_Rekrutmen[sum_rekrutmen] = newRekrutmen;
        sum_rekrutmen += 1;
    }

    function getSumRekrutmen() public view returns (uint) {
        return sum_rekrutmen;
    }

    // Set up data pelamar rekrutmen startup
    struct PelamarStartup {
        address id_pelamar;
        uint is_verified;
        string alasan;
        uint rekrutmen_id;
    }

    mapping(uint => PelamarStartup) public list_pelamar;

    function mendaftarRekrutmen(uint _rekrutmen_id) public {
        uint kuota = List_Rekrutmen[_rekrutmen_id].kuota;
        uint jumlah_pendaftar = List_Rekrutmen[_rekrutmen_id].jumlah_pendaftar;
        // Harus masih ada kuota tersisa
        require((kuota - jumlah_pendaftar) > 0);
        require(List_Rekrutmen[_rekrutmen_id].is_active);
        // string [] initArr;
        // initArr.push("0");
        // initArr.push("-");
        PelamarStartup memory newPelamar = PelamarStartup({
            id_pelamar : msg.sender,
            is_verified : 0,
            alasan : "",
            rekrutmen_id : _rekrutmen_id
        });
        list_pelamar[total_pelamar_all] = newPelamar;
        List_Rekrutmen[_rekrutmen_id].jumlah_pendaftar += 1;
        total_pelamar_all += 1;
    }

    function getSumAllPendaftar() public view returns (uint) {
        return total_pelamar_all;
    }

    function getSumPendaftar(uint rekrutmen) public view returns (uint) {
        return List_Rekrutmen[rekrutmen].jumlah_pendaftar;
    }

    // Untuk admin memverifikasi tahap 1 pendaftaram, sebelum mhs lanjut ke situs utama
    // function verify_pelamar(uint _id_pel, uint status, string alasan) public onlyAdmin() {
    function verify_pelamar(uint _id_pel, uint status, string alasan) public onlyAdmin() {
        list_pelamar[_id_pel].is_verified = status;
        list_pelamar[_id_pel].alasan = alasan;
    }    

    function cek_regist_exist(address pel, uint rekrutmen_id) public view returns (bool){
        bool terdaftar = false;
        for (uint i = 0; i < total_pelamar_all; i++) {
            if (list_pelamar[i].id_pelamar == pel && list_pelamar[i].rekrutmen_id == rekrutmen_id) {
                terdaftar = true;
                break;
            }
        }    
        return terdaftar;
    }

    // Set up pelamar, data diri saja. Bukan data pelamar.
    struct CalonPelamar {
        address id_pel;
        string nama;
        string nik;
        string ttl;
        string alamat;
        string email;
    }

    // address[] public Mhs;
    mapping(address => CalonPelamar) public list_calon_pelamar;
    mapping(uint => CalonPelamar) public list_pelamar_admin;

    function simpanDataDiri(string _nama, string _nik, string _ttl, string _alamat, string _email) public {
        CalonPelamar memory newCalonPelamar = CalonPelamar({
            id_pel : msg.sender,
            nama : _nama,
            nik : _nik,
            ttl : _ttl,
            alamat : _alamat,
            email : _email
        });
        list_calon_pelamar[msg.sender] = newCalonPelamar;
        list_pelamar_admin[jumlah_pelamar] = newCalonPelamar;
        jumlah_pelamar += 1;
    }

    function getSumAllPelamar() public view returns (uint) {
        return jumlah_pelamar;
    }

    function getAllPelamar() public view returns (CalonPelamar[] memory) {
        CalonPelamar[] memory allPelamar = new CalonPelamar[](jumlah_pelamar);
        for (uint i = 0; i < jumlah_pelamar; i++) {
            CalonPelamar storage pel = list_pelamar_admin[i];
            allPelamar[i] = pel;
        }
        return allPelamar;
    }

    function getPelamar(address pel) public view returns (CalonPelamar memory) {
        return list_calon_pelamar[pel];
    }    
}
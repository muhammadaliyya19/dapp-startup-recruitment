import React, { Component } from 'react'  
import { Link } from 'react-router-dom';  
export class SidebarAdmin extends Component {  
    render() {  
        return (  
            <div>  
                <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">  
                <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/">  
                        <div className="sidebar-brand-text mx-3">DAPP Recruitmen </div>  
                    </a>  
  
                    <hr className="sidebar-divider my-0" />  
                    <li className="nav-item">  
                        <a className="nav-link" href="/">  
                            <i className="fas fa-fw fa-home"></i>  
                            <span>Home</span>
                        </a>  
                    </li>
                    <li className="nav-item">  
                        <a className="nav-link" href="/ListRekrutmen">  
                            <i className="fas fa-fw fa-cogs"></i>  
                            <span>Kelola Rekrutmen</span>
                        </a>  
                    </li>
                    <li className="nav-item">  
                        <a className="nav-link" href="/ListPelamar">  
                            <i className="fas fa-fw fa-users"></i>  
                            <span>Data Pelamar</span>
                        </a>
                    </li>  
                    <hr className="sidebar-divider d-none d-md-block" /> 
                </ul>  
            </div>  
        )  
    }  
}  
  
export default SidebarAdmin
import React, { Component } from "react";
// import Evoting from "../contracts/Evoting.json";
// import Beasiswa from "../contracts/Beasiswa.json";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";

import { Button } from 'react-bootstrap';

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';
import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  

import '../index.css';

class MyRegist extends Component {
  constructor(props) {
    super(props)

    this.state = {
      RekrutmenInstance: undefined,
      account: null,
      web3: null,
      // AllRekrutmen : null,
      RegistSaya: null,
      dataDiriku: null,
      jmlPendaftar: null,
      isOwner: false
    }
  }

  componentDidMount = async () => {
    // FOR REFRESHING PAGE ONLY ONCE -
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }

    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Rekrutmen.networks[networkId];
      const instance = new web3.eth.Contract(
        Rekrutmen.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      // this.setState({ web3, accounts, contract: instance }, this.runExample);
      this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

      // Mengambil data diri terlebih dahulu
      let dataDiriku = await this.state.RekrutmenInstance.methods.list_calon_pelamar(this.state.account).call();        
      this.setState({ dataDiriku: dataDiriku });

      // if (dataDiriku[1] !== "") {
        // Kalau dataDiriKu ada maka dilakukan set-state
        // this.setState({ 
            // nama        : dataDiriku[1],
            // nim         : dataDiriku[2],
            // ipk         : dataDiriku[3],
            // ttl         : dataDiriku[4],
            // alamat      : dataDiriku[5],
        // });
      // }

    let jumlah_rekrutmen = await this.state.RekrutmenInstance.methods.getSumRekrutmen().call();
    let daftar_rekrutmen = [];
    for (let i = 0; i < jumlah_rekrutmen; i++) {
        let recruitment = await this.state.RekrutmenInstance.methods.List_Rekrutmen(i).call();
        daftar_rekrutmen.push(recruitment);
    }
      
      //   Populasi pendaftaran saya
    let jumlahPendaftar = await this.state.RekrutmenInstance.methods.getSumAllPendaftar().call();
    let ListPendaftaranSaya = [];
      for (let i = 0; i < jumlahPendaftar; i++) {
        let detailPendaftar = await this.state.RekrutmenInstance.methods.list_pelamar(i).call();        
        if (detailPendaftar.id_pelamar === this.state.account) {
            detailPendaftar.namaRekrutmen = daftar_rekrutmen[detailPendaftar.rekrutmen_id].judul;
            detailPendaftar.linkRekrutmen = daftar_rekrutmen[detailPendaftar.rekrutmen_id].main_link;
            ListPendaftaranSaya.push(detailPendaftar);
        }        
        // let id_mhs = await this.state.RekrutmenInstance.methods.Mhs(i).call();
        
      }
      this.setState({ RegistSaya: ListPendaftaranSaya });      

      // this.setState({ AllRekrutmen: daftar_beasiswa });

    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

//   verifikasi = async event => {
//     await this.state.RekrutmenInstance.methods.memverifikasi(event.target.value).send({ from: this.state.account, gas: 1000000 });    
//     window.location.reload(false);
//   }

  render() {
    let myRegist = [];    
    if (this.state.RegistSaya) {
      if (this.state.RegistSaya.length > 0) {
        for (let j = 0; j < this.state.RegistSaya.length; j++) {
          // SELEKSI Tombol Verif
          let tombol_verif = [];
          let alasan_verif = [];
          // pendaftaran.is_verified='1';
          if (this.state.RegistSaya[j].is_verified==='1') {
            tombol_verif.push(<span className="text-success">Anda Terverifikasi dan Lolos Tahap Pertama</span>);
            tombol_verif.push(<p>Silakan Akses Link Berikut Untuk Melanjutkan Proses Pendaftaran.</p>);
            tombol_verif.push(<a class="nav-link h4 scrollto" target="_blank" href={`https:///${this.state.RegistSaya[j].linkRekrutmen}`}>{this.state.RegistSaya[j].linkRekrutmen}</a>);
            alasan_verif.push("Alasan Kelolosan : ");
            alasan_verif.push(this.state.RegistSaya[j].alasan);
          }else if(this.state.RegistSaya[j].is_verified=='2'){
            tombol_verif.push(<span className="text-danger">Maaf Anda Tidak Lolos Verifikasi</span>);                
            alasan_verif.push(this.state.RegistSaya[j].alasan);
          }else if(this.state.RegistSaya[j].is_verified=='3'){
            tombol_verif.push(<span className="text-success">Selamat! Anda Lolos Rekrutmen { this.state.RegistSaya[j].namaRekrutmen }</span>);                
            alasan_verif.push(this.state.RegistSaya[j].alasan);
          }else if(this.state.RegistSaya[j].is_verified=='4'){
            tombol_verif.push(<span className="text-danger">Maaf Anda Tidak Lolos Rekrutmen</span>);                
            alasan_verif.push(this.state.RegistSaya[j].alasan);
          }else{
            tombol_verif.push(<span className="text-warning">Data Belum Diverifikasi</span>);
            alasan_verif.push("-");
          }

          myRegist.push(
            // <div className="candidate">
            //   <div className="candidateName">{this.state.dataDiriku[1]}</div>
            //   <div className="CandidateDetails">
            //     <div>NIK : {this.state.dataDiriku[2]}</div>
            //     <div>Alamat : {this.state.dataDiriku[4]}</div>
            //     <div>Email : {this.state.dataDiriku[5]}</div>
            //     <div>ID Rekrutmen : {this.state.RegistSaya[j].rekrutmen_id}</div>
            //     <div>Nama Rekrutmen : {this.state.RegistSaya[j].namaRekrutmen}</div>
            //   </div>

            //   <div className="CandidateDetails">
            //   {tombol_verif}
            //   </div>
            //   <div className="CandidateDetails">
            //   {alasan_verif}
            //   </div>
            //   <div><br></br></div>
            // </div>
            <tr>
              <td>{this.state.RegistSaya[j].rekrutmen_id}</td>
              <td>{this.state.RegistSaya[j].namaRekrutmen}</td>
              <td>{this.state.dataDiriku[2]}</td>
              <td>{this.state.dataDiriku[1]}</td>
              <td>{this.state.dataDiriku[3]}</td>
              <td>{this.state.dataDiriku[4]}</td>
              <td>{this.state.dataDiriku[5]}</td>
              <td>{tombol_verif}</td>
              <td>{alasan_verif}</td>
            </tr>
          );
        }
      }      
    }

    if (!this.state.web3) {
      return (
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-12 pl-4">
                  <h1>
                    Loading Web3, accounts, and contract..
                  </h1>
                </div>
              </div>
          </div>
        </div>  
      </div> 
      );
    }

    return (
      <div>
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-6 pl-4">
                  <h4>Lamaran Saya</h4>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  <div className="card m-3">
                    <div className="card-body" style={{ maxHeight: '500px', overflowY: 'scroll' }}>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">ID Rekrutmen</th>
                            <th scope="col">Nama Rekrutmen</th>
                            <th scope="col">NIK</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Tempat / Tanggal Lahir</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">E-Mail</th>
                            <th scope="col">Status</th>
                            <th scope="col">Reason</th>
                          </tr>
                        </thead>
                        <tbody>
                          {myRegist}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
          </div>
            <Footer />
        </div>  
      </div> 
      </div> 
    );
  }
}

export default MyRegist;

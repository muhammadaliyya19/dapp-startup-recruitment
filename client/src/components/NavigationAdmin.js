import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NavigationAdmin extends Component {
    render() {
        return (
            // <div className='navbar'>
            //     <div className="Admin">ADMIN</div>
            //     <Link to='/' className="heading">HOME</Link>
            //     <Link to='/ListRekrutmen'>KELOLA BEASISWA</Link>
            // </div>
            <header id="header" className="d-flex align-items-center">
                <div className="container d-flex align-items-center justify-content-between">
                <h1 className="logo"><a href="/">DAPP Start Up Recruitment</a></h1>
                <nav id="navbar" className="navbar">
                    <ul>
                    <li><a className="nav-link scrollto" href="/">HOME</a></li>
                    <li><a className="nav-link scrollto" href="/ListRekrutmen">KELOLA REKRUTMEN</a></li>                 
                    <li><a className="nav-link scrollto" href="/ListPelamar">DATA PELAMAR</a></li>                 
                    </ul>
                    <i className="bi bi-list mobile-nav-toggle"></i>
                </nav>

                </div>
            </header>
        );
    }
}

export default NavigationAdmin;
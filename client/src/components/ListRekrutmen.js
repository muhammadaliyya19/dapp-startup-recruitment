import React, { Component } from "react";
// import Rekrutmen from "../contracts/Rekrutmen.json";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";

import '../index.css';

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';
import { Link } from 'react-router-dom';

import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  
class ListRekrutmen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      // EvotingInstance: undefined,
      RekrutmenInstance: undefined,
      account: null,
      web3: null,
      jumlah_rekrutmen: 0,
      total_pelamar: 0,
      listRekrutmen: null,
      loaded: false,
      isOwner: false,
      statusterdaftar: []
    }
  }

  // getCandidates = async () => {
  //   let result = await this.state.EvotingInstance.methods.getCandidates().call();

  //   this.setState({ candidates : result });
  //   for(let i =0; i <result.length ; i++)


  // }

  componentDidMount = async () => {

    // FOR REFRESHING PAGE ONLY ONCE -
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }

    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Rekrutmen.networks[networkId];
      const instance = new web3.eth.Contract(
        Rekrutmen.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      // this.setState({ web3, accounts, contract: instance }, this.runExample);
      this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

      let jumlah_rekrutmen = await this.state.RekrutmenInstance.methods.getSumRekrutmen().call();
      this.setState({ jumlah_rekrutmen: jumlah_rekrutmen });

      let jumlah_pelamar = await this.state.RekrutmenInstance.methods.getSumAllPendaftar().call();
      this.setState({ jumlah_pelamar: jumlah_pelamar });

      let daftar_rekrutmen = [];
      let stateterdaftar = [];
      // for (let j = 0; j < 10; j++) {
      for (let i = 0; i < jumlah_rekrutmen; i++) {
        let rekrutmen = await this.state.RekrutmenInstance.methods.List_Rekrutmen(i).call();
        var terdaftar = await this.state.RekrutmenInstance.methods.cek_regist_exist(this.state.account, i).call();
        daftar_rekrutmen.push(rekrutmen);
        stateterdaftar.push(terdaftar);
      }
      // }

      this.setState({ listRekrutmen: daftar_rekrutmen });
      this.setState({ statusterdaftar: stateterdaftar });

      const owner = await this.state.RekrutmenInstance.methods.Admin().call();
      if (this.state.account === owner) {
        this.setState({ isOwner: true });
      }

    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }

    function doSearch(keyword) {
      this.listRekrutmen.forEach(rec => {
        console.log("ketemu", rec)
      });
    }
  };

  

  render() {
    // return (
    //   <div>
    //     <div id="wrapper">
    //       <Sidebar></Sidebar>
    //       <div id="content-wrapper" class="d-flex flex-column">
    //         <div id="content">
    //           <Header />
    //           <div>
                
    //           </div>
    //         </div>
    //         <Footer />
    //       </div>
    //     </div>  
    //   </div> 
    // );

    let daftarRekrutmen;
    if (this.state.listRekrutmen) {
      daftarRekrutmen = this.state.listRekrutmen.map((Data_Rekrutmen) => {
        var sisa_kuota = Data_Rekrutmen.kuota - Data_Rekrutmen.jumlah_pendaftar;
        return (
          // <div className="candidate">
          //   <div className="candidateName">
          //     {Data_Rekrutmen.judul} || 
          //     {
          //     this.state.isOwner ? (
          //       <Link to={`VerifyRegist/${Data_Rekrutmen.id}`}> Verifikasi Pendaftar </Link> 
          //     ) : (
          //       this.state.statusterdaftar[Data_Rekrutmen.id] ? (
          //         <span className="text-success"> Anda Sudah Mendaftar Rekrutmen Ini</span>
          //       ) : (
          //         sisa_kuota > 0 ? (
          //           <Link to={`RegistRekrutmen/${Data_Rekrutmen.id}`}> Lamar Rekrutmen</Link>
          //         ) : (
          //           <span className="text-danger"> Kuota Sudah Terpenuhi </span>
          //         )
          //       )
          //     )}
          //   </div>
          //   <div className="CandidateDetails">
          //     <div>Deskripsi : {Data_Rekrutmen.deskripsi}</div>
          //     <div>
          //       Kuota Pelamar : {Data_Rekrutmen.kuota}
          //       <br></br> 
          //       {this.state.isOwner ? "Jumlah Pelamar : " + Data_Rekrutmen.jumlah_pendaftar : "Kuota Tersisa : " + sisa_kuota }                
          //     </div>
          //     <div>                
          //       {this.state.isOwner ? "Link Utama :" + Data_Rekrutmen.main_link : ""}
          //     </div>
          //     <div>Kode Rekrutmen : {Data_Rekrutmen.id}</div>
          //   </div>
          // </div>
          <tr>
            <td>{Data_Rekrutmen.judul}</td>
            <td>{Data_Rekrutmen.deskripsi}</td>
            <td>
              {"Kuota Tersedia : " + Data_Rekrutmen.kuota}<br></br>
              {this.state.isOwner ? "Jumlah Pelamar : " + Data_Rekrutmen.jumlah_pendaftar : "Kuota Tersisa : " + sisa_kuota }
            </td>
            {this.state.isOwner ? <td> Data_Rekrutmen.main_link </td> : ""}
            <td>{Data_Rekrutmen.id}</td>
            <td>
              {
                this.state.isOwner ? (
                  <Link to={`VerifyRegist/${Data_Rekrutmen.id}`} className="btn btn-sm btn-primary"> Verifikasi Pendaftar </Link> 
                ) : (
                  this.state.statusterdaftar[Data_Rekrutmen.id] ? (
                    <span className="text-success"> Anda Sudah Mendaftar Rekrutmen Ini</span>
                  ) : (
                    sisa_kuota > 0 ? (
                      <Link className="btn btn-md btn-success" to={`RegistRekrutmen/${Data_Rekrutmen.id}`}> Lamar </Link>
                    ) : (
                      <span className="text-danger"> Kuota Sudah Terpenuhi </span>
                    )
                  )
                )
              }
            </td>
          </tr>
        );
      });
    }

    if (!this.state.web3) {
      return (
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-12 pl-4 text-center">
                  <h1>
                    Loading Web3, accounts, and contract..
                  </h1>
                </div>
              </div>
          </div>
        </div>  
      </div> 
      );
    }

    return (
      // <div className="CandidateDetails">
      //   {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
      //   <div className="CandidateDetails-title">
      //     <h1>
      //       {this.state.isOwner ? "KELOLA REKRUTMEN || " : "INFO REKRUTMEN"}            
      //       {this.state.isOwner ? <Link to='/AddRekrutmen' className="btn btn-md btn-primary">Tambah Rekrutmen</Link> : ""}            
      //     </h1>
      //   </div>


      //   <div className="section-title">
      //     <h3>Total Jumlah Rekrutmen : <span>{this.state.jumlah_rekrutmen}</span></h3>
      //     {this.state.isOwner ? <h3>Total Jumlah Pelamar : <span>{this.state.jumlah_pelamar}</span></h3>: ""}                              
      //   </div>
      //   <div>
      //     {daftarRekrutmen}
      //   </div>
      // </div>

      // New Layout
      <div>
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-4 pl-4">
                  <h4>Informasi Rekrutmen</h4>
                </div>
                <div className="col-lg-4 text-right pr-4">
                  <input type="text" onChange={this.doSearch}></input>
                </div>
                <div className="col-lg-4 text-right pr-4">
                  {this.state.isOwner ? <Link to='/AddRekrutmen' className="btn btn-sm btn-primary"><i className="fa fa-plus"></i> Tambah</Link> : ""}
                </div>
              </div>
              <div className="card m-3">
                <div className="card-body" style={{ maxHeight: '500px', overflowY: 'scroll' }}>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Judul</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Kuota</th>
                        {this.state.isOwner ? <th scope="col">Link Utama</th> : "" }
                        <th scope="col">Kode Rekrutmen</th>
                        <th scope="col">Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      {/* <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                      </tr> */}
                      {daftarRekrutmen}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>  
      </div> 
    );
  }
}

export default ListRekrutmen;

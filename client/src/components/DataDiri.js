import React, { Component } from "react";
// import Rekrutmen from "../contracts/Rekrutmen.json";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';

import { FormGroup, FormControl, Button } from 'react-bootstrap';
import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  
class DataDiri extends Component {
  constructor(props) {
    super(props)

    this.state = {
      RekrutmenInstance: undefined,
      account: null,
      web3: null,
      registered: false,
      nama: '',
      nik: '',
      ttl: '',
      alamat: '',
      email: '',
      isOwner: false
    }
  }

  updateNama = event => {
    this.setState({ nama: event.target.value });
  }

  updateNIK = event => {
    this.setState({ nik: event.target.value });
  }

  // updateIPK = event => {
  //   this.setState({ ipk: event.target.value });
  // }

  updateTTL = event => {
    this.setState({ ttl: event.target.value });
  }

  updateAlamat = event => {
    this.setState({ alamat: event.target.value });
  }

  updateEmail = event => {
    this.setState({ email: event.target.value });
  }

  simpanDataDiri = async () => {
    await this.state.RekrutmenInstance.methods.simpanDataDiri(this.state.nama, this.state.nik, this.state.ttl, this.state.alamat, this.state.email).send({ from: this.state.account, gas: 1000000 });
    // Reload
    window.location.href= '/DataDiri';
        // window.location.reload(false);
  }

  // ubahDataDiri = async () => {
  //   await this.state.RekrutmenInstance.methods.ubahDataDiri(this.state.nama, this.state.nim, this.state.ipk,
  //     this.state.ttl, this.state.alamat, this.state.email).send({ from: this.state.account, gas: 1000000 });
  //   // Reload
  //   window.location.href= '/DataDiri';
  //       // window.location.reload(false);
  // }

  componentDidMount = async () => {
    // FOR REFRESHING PAGE ONLY ONCE -
    if (!window.location.hash) {
      window.location = window.location + '#loaded';
      window.location.reload();
    }
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Rekrutmen.networks[networkId];
      const instance = new web3.eth.Contract(
        Rekrutmen.abi,
        deployedNetwork && deployedNetwork.address,
      );
      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.

      // this.setState({ web3, accounts, contract: instance }, this.runExample);
      this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

      // let jumlahPemilih = await this.state.RekrutmenInstance.methods.mendapatkanJumlahPemilih().call();
      
    
    // Get Data Diri
    // console.log('load data diri');
    let dataDiriku = await this.state.RekrutmenInstance.methods.list_calon_pelamar(this.state.account).call();        
    // console.log(dataDiriku);
    // console.log(dataDiriku[1] == "");
    if (dataDiriku[1] !== "") {
        // Kalau dataDiriKu ada maka dilakukan set-state
        this.setState({ 
            nama        : dataDiriku[1],
            nik         : dataDiriku[2],
            // ipk         : dataDiriku[3],
            ttl         : dataDiriku[3],
            alamat      : dataDiriku[4],
            email       : dataDiriku[5],
            registered  : true,
        });
    }

    const owner = await this.state.RekrutmenInstance.methods.Admin().call();
        if (this.state.account === owner) {
            this.setState({ isOwner: true });
        }
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  render() {
    if (!this.state.web3) {
      return (
        <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-12 px-4 text-center">
                  <h1>
                    Loading Web3, accounts, and contract..
                  </h1>
                </div>
              </div>
            </div>
          </div>  
        </div> 
      );
    }

    return (
      <div id="wrapper">
          {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
          <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
              <Header />
              <div className="row">
                <div className="col-lg-6 px-4">
                  <h4>Data Diri</h4>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 px-4">
                  <div className="card">
                    <div className="card-body" style={{ maxHeight: '500px', overflowY: 'scroll' }}>
                  
                      {!this.state.registered ? (<h4 className="text-danger"> Silakan Lengkapi Data Diri </h4>) : ( "" )}
                  
                      <FormGroup>
                          <label>Nama</label>
                          <div className="form-input">
                              <FormControl
                                  input='text'
                                  value={this.state.nama}
                                  onChange={this.updateNama}
                              />
                          </div>

                          <label>NIK</label>
                          <div className="form-input">
                              <FormControl
                                  input='text'
                                  value={this.state.nik}
                                  onChange={this.updateNIK}
                              />
                          </div>        
                      
                          <label>Tempat / Tanggal Lahir</label>
                          <div className="form-input">
                              <FormControl
                                  input='text'
                                  value={this.state.ttl}
                                  onChange={this.updateTTL}
                              />
                          </div>        
                      
                          <label>Alamat</label>
                          <div className="form-input">
                              <FormControl
                                  input='text'
                                  value={this.state.alamat}
                                  onChange={this.updateAlamat}
                              />
                          </div>        
                      
                          <label>E-Mail</label>
                          <div className="form-input">
                              <FormControl
                                  input='text'
                                  value={this.state.email}
                                  onChange={this.updateEmail}
                              />
                          </div>        
                      </FormGroup>
                      {this.state.registered ? "" : 
                        <Button onClick={this.simpanDataDiri} className="btn btn-success btn-md float-right">
                            {"Simpan Data Diri"}
                        </Button>                
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </div>  
      </div> 

        // <div className="">
        //     <div className="CandidateDetails">
        //         {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
        //         <div className="CandidateDetails-title">
        //             <h1>DATA DIRI PELAMAR</h1>
        //         </div>
        //     </div>
        //     <br></br>
        //     <div className="row">
        //     <div className="col-lg-1"></div>
        //     <div className="col-lg-10">
            
            // {!this.state.registered ? (<span className="h4 text-danger"> Silakan Lengkapi Data Diri </span>) : ( "" )}
            
            // <FormGroup>
            //     <label>Nama</label>
            //     <div className="form-input">
            //         <FormControl
            //             input='text'
            //             value={this.state.nama}
            //             onChange={this.updateNama}
            //         />
            //     </div>
            // </FormGroup>         
            // <FormGroup>
            //     <label>NIK</label>
            //     <div className="form-input">
            //         <FormControl
            //             input='text'
            //             value={this.state.nik}
            //             onChange={this.updateNIK}
            //         />
            //     </div>        
            // </FormGroup>  
            // <FormGroup>
            //     <label>Tempat / Tanggal Lahir</label>
            //     <div className="form-input">
            //         <FormControl
            //             input='text'
            //             value={this.state.ttl}
            //             onChange={this.updateTTL}
            //         />
            //     </div>        
            // </FormGroup>
            // <FormGroup>
            //     <label>Alamat</label>
            //     <div className="form-input">
            //         <FormControl
            //             input='text'
            //             value={this.state.alamat}
            //             onChange={this.updateAlamat}
            //         />
            //     </div>        
            // </FormGroup>
            // <FormGroup>
            //     <label>E-Mail</label>
            //     <div className="form-input">
            //         <FormControl
            //             input='text'
            //             value={this.state.email}
            //             onChange={this.updateEmail}
            //         />
            //     </div>        
            // </FormGroup>
            // {this.state.registered ? "" : 
            //   <Button onClick={this.simpanDataDiri} className="button-vote">
            //       {"Simpan Data Diri"}
            //   </Button>                
            // }
        //   </div>
        //   <div className="col-lg-1"></div>                
          
        //   </div>


        // </div>
    );
      
    
  }
}

export default DataDiri;

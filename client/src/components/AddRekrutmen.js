import React, { Component } from "react";
// import Beasiswa from "../contracts/Beasiswa.json";
import Rekrutmen from "../contracts/Rekrutmen.json";
import getWeb3 from "../getWeb3";
import emailjs from '@emailjs/browser';
import CKEditor from 'react-ckeditor-component';

import { FormGroup, FormControl, Button } from 'react-bootstrap';

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';
import Sidebar from '../layouts/Sidebar';  
import SidebarAdmin from '../layouts/SidebarAdmin';  
import Header from '../layouts/Header'  
import Footer from '../layouts/Footer'  

class AddRekrutmen extends Component {
    constructor(props) {
        super(props)

        this.state = {
            RekrutmenInstance: undefined,
            account: null,
            web3: null,
            judul: '',
            deskrpsi: '',
            main_link: '',
            kuota: ''
        }
    }

    updateJudul = event => {
        this.setState({ judul: event.target.value });
    }

    updateDeskripsi = event => {
        this.setState({ deskrpsi: event.target.value });
    }

    updateMainLink = event => {
        this.setState({ main_link: event.target.value });
    }

    updateKuota = event => {
        console.log(event.target.value);
        this.setState({ kuota: event.target.value });
    }

    sendNotifEmail = event => {
        // event.preventDefault();
        let data_email = {
            judul: this.state.judul,
            deskripsi: this.state.deskrpsi,
            kuota: this.state.kuota,
            from_name: 'DAPP REKRUTMEN Start Up',
            to_name:'Pelamar Rekrutmen Start Up',
            to_email:'muhammadasaadilhaqisya@gmail.com, aliyyailmi20@gmail.com, muhammad1998@student.ub.ac.id',
            reply_to:'muhammadasaadilhaqisya@gmail.com',
            message: 'New message tester',
        };
        emailjs.send('service_vqcdpvs', 'template_jd5by05', data_email, 'wnFRXLi4rM8nlLcGt');
    }

    penambahanRekrutmen = async () => {
        await this.state.RekrutmenInstance.methods.penambahanRekrutmen(this.state.judul, this.state.deskrpsi, this.state.main_link, this.state.kuota).send({ from: this.state.account, gas: 1000000 });
        console.log('OTW send email');
        this.sendNotifEmail();
        console.log('Beres send email');
        window.location.href= '/ListRekrutmen';
    }

    componentDidMount = async () => {

        // FOR REFRESHING PAGE ONLY ONCE -
        if (!window.location.hash) {
            window.location = window.location + '#loaded';
            window.location.reload();
        }
        try {
            // Get network provider and web3 instance.
            const web3 = await getWeb3();

            // Use web3 to get the user's accounts.
            const accounts = await web3.eth.getAccounts();

            // Get the contract instance.
            const networkId = await web3.eth.net.getId();
            const deployedNetwork = Rekrutmen.networks[networkId];
            const instance = new web3.eth.Contract(
                Rekrutmen.abi,
                deployedNetwork && deployedNetwork.address,
            );
            // Set web3, accounts, and contract to the state, and then proceed with an
            // example of interacting with the contract's methods.

            this.setState({ RekrutmenInstance: instance, web3: web3, account: accounts[0] });

            const owner = await this.state.RekrutmenInstance.methods.Admin().call();
            if (this.state.account === owner) {
                this.setState({ isOwner: true });
            }

        } catch (error) {
            // Catch any errors for any of the above operations.
            alert(
                `Failed to load web3, accounts, or contract. Check console for details.`,
            );
            console.error(error);
        }
    };

    render() {
        if (!this.state.web3) {
            return (
                <div id="wrapper">
                    {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
                    <div id="content-wrapper" class="d-flex flex-column">
                        <div id="content">
                            <Header />
                            <div className="row">
                                <div className="col-lg-12 pl-4 text-center">
                                    <h1>
                                        Loading Web3, accounts, and contract..
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div> 
            );
        }

        if (!this.state.isOwner) {
            return (
                <div id="wrapper">
                    {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
                    <div id="content-wrapper" class="d-flex flex-column">
                        <div id="content">
                        <Header />
                        <div className="row">
                            <div className="col-lg-12 pl-4 text-center">
                            <h1>
                                HANYA ADMIN YANG DAPAT MENGAKSES
                            </h1>
                            </div>
                        </div>
                    </div>
                    </div>  
                    <Footer />
                </div> 
            );
        }
        return (
            // <div className="">

            //     <div className="CandidateDetails">
            //     {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
            //         <div className="CandidateDetails-title">
            //             <h1>
            //                 Penambahan Rekrutmen
            //             </h1>
            //         </div>
            //     </div>
            //     <br></br>
            //     <div className="row">
            //     <div className="col-lg-1"></div>
            //     <div className="col-lg-10">
            //         <FormGroup>
            //             <label>Masukan Judul</label>
            //             <div className="form-input">
            //                 <FormControl
            //                     input='text'
            //                     value={this.state.judul}
            //                     onChange={this.updateJudul}
            //                 />
            //             </div>
            //         </FormGroup>

            //         <FormGroup>
            //             <label>Main Link</label>
            //             <div className="form-input">
            //                 <FormControl
            //                     input='text'
            //                     value={this.state.main_link}
            //                     onChange={this.updateMainLink}
            //                 />
            //             </div>
            //         </FormGroup>

            //         <FormGroup>
            //             <label>Kuota</label>
            //             <div className="form-input">
            //                 <FormControl
            //                     input='text'
            //                     value={this.state.kuota}
            //                     onChange={this.updateKuota}
            //                 />
            //             </div>
            //         </FormGroup>

            //         <FormGroup>
            //         <label>Deskripsi</label>
            //         <div className="form-input">
            //                 <FormControl
            //                     input='text'
            //                     value={this.state.deskrpsi}
            //                     onChange={this.updateDeskripsi}
            //                 />
            //             </div>
            //         {/* <CKEditor 
            //             activeClass="editor"
            //             content={this.state.deskrpsi} 
            //             events={{
            //                 "change": this.updateDeskripsi
            //             }}
            //         /> */}
                    
            //         </FormGroup>                    
            //         <br></br>
            //         <Button onClick={this.penambahanRekrutmen} className="btn btn-md btn-primary">
            //             Tambah
            //         </Button>
            //         <br></br>
            //         <br></br>
            //     </div>
            //     <div className="col-lg-1"></div>                
            //     </div>

            // </div>
            <div id="wrapper">
                {this.state.isOwner ? <SidebarAdmin /> : <Sidebar />}
                <div id="content-wrapper" class="d-flex flex-column">
                    <div id="content">
                        <Header />
                        <div className="row">
                            <div className="col-lg-1"></div>
                            <div className="col-lg-10">
                                <FormGroup>
                                    <label>Masukan Judul</label>
                                    <div className="form-input">
                                        <FormControl
                                            input='text'
                                            value={this.state.judul}
                                            onChange={this.updateJudul}
                                        />
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <label>Main Link</label>
                                    <div className="form-input">
                                        <FormControl
                                            input='text'
                                            value={this.state.main_link}
                                            onChange={this.updateMainLink}
                                        />
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <label>Kuota</label>
                                    <div className="form-input">
                                        <FormControl
                                            input='text'
                                            value={this.state.kuota}
                                            onChange={this.updateKuota}
                                        />
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                <label>Deskripsi</label>
                                <div className="form-input">
                                        <FormControl
                                            input='text'
                                            value={this.state.deskrpsi}
                                            onChange={this.updateDeskripsi}
                                        />
                                    </div>
                                {/* <CKEditor 
                                    activeClass="editor"
                                    content={this.state.deskrpsi} 
                                    events={{
                                        "change": this.updateDeskripsi
                                    }}
                                /> */}
                                
                                </FormGroup>                    
                                <br></br>
                                <Button onClick={this.penambahanRekrutmen} className="btn btn-md btn-primary float-right">
                                    Tambah
                                </Button>
                            </div>
                            <div className="col-lg-1"></div>
                        </div>
                    </div>
                    <Footer />
                    </div>  
            </div> 

        );
    }
}

export default AddRekrutmen;
